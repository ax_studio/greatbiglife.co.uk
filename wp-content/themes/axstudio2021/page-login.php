<?php

/*
Template Name: AX - Login Page
*/

get_header();
?>
<?php if (!is_user_logged_in()) { ?>

    <main class="login">

        <section>
            <div class="login-container">
                <div class="padding-default-0">
                    <h1 class="text-center">Log In</h1>
                    <br/>


                    <?php
                    $args = array(
                        'redirect' => home_url() . '/login/',
                        'form_id' => 'loginform-custom',
                        'label_username' => __('Email Address'),
                        'label_password' => __('Password'),
                        'label_remember' => __('Remember Me'),
                        'label_log_in' => __('Log In'),
                        'remember' => true
                    ); ?>

                    <?php wp_login_form($args); ?>
                    <p class="small text-align-center"><a href="/forgot-password">Lost your
                            password?</a></p>

                </div>
            </div>

        </section>

    </main>
<?php } ?>

<?php if (is_user_logged_in()) { ?>
    <?php if (current_user_can('client') || current_user_can('business_partner')) { ?>
        <?php wp_redirect(site_url() . '/portal/', 301);
        exit; ?>
    <?php } ?>

    <?php if (current_user_can('administrator') || current_user_can('admin')) { ?>
        <?php wp_redirect(site_url() . '/admin/', 301);
        exit; ?>
    <?php } ?>
<?php } ?>

<?php
get_footer(); ?>
