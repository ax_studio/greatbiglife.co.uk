<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AX_studio
 */

?>

</div>

<div class="cookie-compliance">
    <div class="container">
        <p>We use "cookies" (own or third parties authorized) for analytical purposes and to show you personalized advertising based on a profile of your web surfing habits (for example, visited pages). More information on our <a href="/cookies-policy/" class="text-decoration-underline">Cookie Policy</a>.</p>
        <div class="text-align-center">
            <a class="cookie-compliance__submit">Accept</a>
        </div>

    </div>
</div>
<footer class="footer">
<!--    <div id="footer-navigation" class="footer-navigation">-->
<!--        --><?php
//        wp_nav_menu(array(
//            'theme_location' => 'footer',
//            'menu_id' => 'footer-menu',
//        ));
//        ?>
<!--    </div>-->

    <div class="footer-copyright">
        <p class="small">© <?php echo date("Y") . ' '. get_bloginfo( 'name' ); ?> | Website created by <a href="https://axstudio.uk" target="_blank">AX Studio</a></p>
    </div>
</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
