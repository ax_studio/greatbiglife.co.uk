function textareaAutoGrow() {


    $('textarea').attr('rows', '1');

    $('textarea').each(function () {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
}

function onScroll() {
    $(window).scroll(function () {

        if ($(this).scrollTop() > 70) {
        } else {
        }
    });
}



function cookieBanner() {
    /* show cookie policy */
    'use strict';

    document.addEventListener("DOMContentLoaded", function() {
        showCookiePolicy();
    });


    function showCookiePolicy() {
        if (!getCookie()) {
            var btn = document.querySelector('.cookie-compliance__submit');

            btn.addEventListener('click', function(e) {
                e.preventDefault();
                setCookie();
                hideCookiePolicy();
            });
            setTimeout(() => {
                document.documentElement.classList.add('js-show-cookie-banner');
            }, 3000);
        }
    }

    function hideCookiePolicy() {
        document.documentElement.classList.remove('js-show-cookie-banner');
    }

    function getCookie() {
        return /(^|;)\s*policy=/.test(document.cookie);
    }

    function setCookie() {
        var date = new Date()
        date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000))
        document.cookie = `policy=1; expires=${date.toUTCString()}; path=/`
    }
}

//
// function headerScrollAppear() {
//     var didScroll;
//     var lastScrollTop = 0;
//     var delta = 0;
//     var navbarHeight = $('.header').outerHeight();
//
//     $(window).scroll(function (event) {
//         didScroll = true;
//     });
//
//     setInterval(function () {
//         if (didScroll) {
//             hasScrolled();
//             didScroll = false;
//         }
//     }, 300);
//
//     function hasScrolled() {
//         var st = $(this).scrollTop();
//
//         if (!$('.header-toggle_menu').hasClass('active')) {
//             // Make sure they scroll more than delta
//             if (Math.abs(lastScrollTop - st) <= delta)
//                 return;
//
//             // If they scrolled down and are past the navbar, add class .nav-up.
//             // This is necessary so you never see what is "behind" the navbar.
//             if (st > lastScrollTop && st > navbarHeight) {
//
//                 if (st > 300) {
//                     $('.header').addClass('slideUp');
//                 } else {
//                     $('.header').addClass('slideUp');
//                 }
//
//                 // Scroll Down
//
//             } else {
//                 // Scroll Up
//                 if (st + $(window).height() < $(document).height()) {
//                     $('.header').removeClass('slideUp');
//                 }
//             }
//         }
//
//         lastScrollTop = st;
//     }
// }