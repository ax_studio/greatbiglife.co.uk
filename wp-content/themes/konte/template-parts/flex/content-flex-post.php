<?php
/**
 * Template part for displaying flex post - type post
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Konte
 */

$design  = get_post_meta( get_the_ID(), 'flex_post_design', true );
$post_id = get_post_meta( get_the_ID(), 'flex_post_post', true );

if ( has_post_thumbnail() ) {
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
	$ratio     = $thumbnail[1] / $thumbnail[2];

	if ( $ratio >= 1.3 ) {
		$class = 'thumbnail-landscape';
	} elseif ( $ratio <= 0.8 ) {
		$class = 'thumbnail-portrait';
	} else {
		$class = 'thumbnail-square';
	}
}
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'flex-post--post' ); ?> <?php echo ! empty( $design['css'] ) ? 'style="' . esc_attr( wp_unslash( $design['css'] ) ) . '"' : '' ?>>
	<?php if ( ! empty( $design['background_image'] ) ) : ?>
		<div class="flex-post-background">
			<img src="<?php echo esc_url( $design['background_image'] ); ?>" alt="<?php the_title_attribute(); ?>">
		</div>
	<?php endif; ?>

	<div class="flex-post-content">
		<?php
		$tags = wp_get_post_terms( get_the_ID(), 'flex_post_tag', array( 'fields' => 'names' ) );
		if ( $tags && ! is_wp_error( $tags ) ) {
			echo '<span class="flex-tags">' . implode( ', ', $tags ) . '</span>';
		}
		?>

		<?php
		global $post;
		$post = $flex_post_post = get_post( $post_id );
		setup_postdata( $flex_post_post );
		?>
		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
			<?php
			the_post_thumbnail( 'konte-post-thumbnail-medium', array(
				'alt' => the_title_attribute( array( 'echo' => false ) ),
			) );
			?>
		</a>
		<div class="cat-links" style="<?php echo ! empty( $design['tag_color'] ) ? 'color:' . esc_attr( $design['tag_color'] ) : ''; ?>"><?php the_category( ', ' ); ?></div>
		<h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<div class="post-summary"><?php the_excerpt(); ?></div>
		<a href="<?php the_permalink(); ?>" class="read-more"><?php esc_html_e( 'Continue Reading', 'konte' ) ?></a>
	</div>
</div><!-- #post-<?php the_ID(); ?> -->
