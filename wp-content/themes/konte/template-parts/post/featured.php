<?php
/**
 * Template part for displaying featured post carousel
 *
 * @package Konte
 */

$post_ids      = konte_get_featured_post_ids();
$carousel_type = konte_get_option( 'blog_featured_display' );

if ( empty( $post_ids ) ) {
	return;
}

global $post;
?>

<div id="featured-content" class="featured-content posts-<?php echo esc_attr( $carousel_type ); ?>">
	<div class="konte-container">
		<div id="featured-content-carousel" class="featured-content-carousel <?php echo esc_attr( $carousel_type ); ?>" data-effect="<?php echo esc_attr( konte_get_option( 'blog_featured_slider_effect' ) ) ?>" <?php echo is_rtl() ? 'dir="rtl"' : ''; ?>>
			<?php foreach ( $post_ids as $post_id ) : ?>
				<?php
				$post = get_post( $post_id );
				setup_postdata( $post_id );
				$thumbnail_url = has_post_thumbnail() ? get_the_post_thumbnail_url( null, 'full' ) : '';
				?>

				<div class="featured-item loading" <?php echo ! empty( $thumbnail_url ) ? 'data-lazy="' . esc_attr( $thumbnail_url ) . '"' : ''; ?>>
					<div class="entry-header">
						<div class="cat-links">
							<?php the_category( ', ', '' ); ?>
						</div>

						<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

						<a href="<?php the_permalink(); ?>" class="read-more"><?php esc_html_e( 'Continue reading', 'konte' ) ?></a>
					</div>
				</div>

			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
</div>