<?php
/**
 * Template part for displaying the campaign bar
 *
 * @package Konte
 */

$campaigns = array_filter( (array) konte_get_option( 'campaign_items' ) );

?>
<div id="campaign-bar" class="campaign-bar">
	<div class="<?php echo esc_attr( apply_filters( 'konte_campaigns_container_class', 'konte-container-fluid' ) ); ?>">
		<div class="campaign-bar__campaigns">
			<?php
			foreach ( $campaigns as $campaign ) {
				konte_campaign_item( $campaign );
			}
			?>
		</div>
	</div>
</div>