<?php
/**
 * Template part for displaying the search icon
 *
 * @package Konte
 */
?>
<div class="header-hamburger hamburger-menu" data-target="hamburger-fullscreen">
	<div class="hamburger-box">
		<div class="hamburger-inner"></div>
	</div>
</div>
