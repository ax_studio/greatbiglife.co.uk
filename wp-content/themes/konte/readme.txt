=== Konte ===

Contributors: uixthemes
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready

Requires at least: 4.0
Tested up to: 5.5.3
Stable tag: 1.8.1
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Konte is a fully responsive Premium WordPress Theme with a pixel perfect design and extensive functionality.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Changelog ==

= Version 1.8.1 =
* Update - Update plugins.
* Fix - Incorrect featured products in side products of product layout v7.
* Fix - CSS issue with plugin WC Bundled Product.
* Fix - Missing font files in the editor.
* Fix - Multiple h1 tags on homepage.

= Version 1.8.0 =
* New - Add new shortcodes for Instagram photos.
* Update - Update plugins.
* Update - Update included template files.
* Fix - Maintenance page issue if it is set as a special page.
* Fix - Issue of large logo image on mobile.
* Fix - Quantity dropdown (on product layout v4) issue with variable products.

= Version 1.7.4 =
* Update - Update plugins.
* Update - Update included template files.
* New - Add a new options of floating cart icon.
* Fix - Issue of password field in the login and registration form.

= Version 1.7.3 =
* Update - Update plugins.
* Update - Update included template files.
* Enchancement - Add a new options of checkboxes list display for Products Filter widget.
* Fix - Instagram issue returns error in some special situations.

= Version 1.7.2 =
* Update - Update plugins.
* Fix - Fix Instagram feed issue following new API of Instagram.
* Fix - Blog layout issue when blog sidebar is empty.
* Fix - Portfolio gallery layout issue.
* Fix - Compatible issue with [products] shortcode of WooCommerce.

= Version 1.7.1 =
* Update - Update plugins.
* Fix - Wrong position of product badges on product layout v4 if product has only one image.
* Fix - Products quick search displays no product if set to display categories on shop page.
* Fix - Logo display issue on mobile.
* Fix - Display issue of empty shopping the cart.
* Fix - Incorrect position of preloader with default preloader.
* Fix - Not working typography setting for blog post title.

= Version 1.7.0 =
* Update - Update plugins.
* Update - Included templates.
* New - Add new option to display account icon on header.
* Enhancement - Remove supporting Google+ and add new support for Linkedin.
* Enhancement - Add new options to set the text color for header and footer with Splited Content page template.
* Enhancement - Add a new option to set zoom level of map on page header.
* Fix - Issue of top level menu links with header v10.
* Fix - Jetpack lazy loads compatible issue. Now support both new and old methods of Jetpack lazy loads.
* Fix - Compatible issues with some filter plugins.
* Fix - Update Swiper slider script to fix compatible issues with other plugins.

= Version 1.6.4 =
* Update - Update plugins.
* Enhancement - Update the builtin breadcrumbs schema.
* Enhancement - Update list of translateable options, meta.
* Enhancement - Add font-display: swap to the font.
* Fix - Issue of unscrollable options in the dropdown of product filter.
* Fix - Js error of product quantity dropdown on IE.
* Fix - Incorrect new products group.
* Fix - Issue of typography settings for menu not working with header v10.
* Fix - Issue of slidedown sub-menus with header v10.
* Fix - The layout issue of product masonry element on mobile.

= Version 1.6.3 =
* Update - Update plugins.
* Update - Update WooCommerce templates to version 4.0.0
* Enhancement - Auto close the products filter panel.
* Enhancement - Add new options to stop chaning URL with ajax pagination.
* Enhancement - Add a new option to display product thumbnail on checkout page.
* Fix - Issue with the color of subtitle in page header.
* Fix - Products search returns additional content of the shop page.
* Fix - Issues with RTL.
* Fix - Image swatches cannot display tooltip.
* Fix - Position issue of products filter dropdown.
* Fix - Dropdown on topbar is invisible when sticky header is enabled.

= Version 1.6.2 =
* Update - Update plugins.
* Improve - Improve the style of the backorder notification.
* Enhancement - Support new icons (Amazon, Snapchat, Telegram and Slack) in the social menu.
* Enhancement - Support more characters of other languages.
* Fix - Issue of Product Tabs with carousel while having 1 product only.
* Fix - Can't add more than one product with ajax when the cache is installed.
* Fix - Shop masonry layout while having one 1 product.
* Fix - Layout issue of the mini cart on RTL.
* Fix - Login page style on RTL.
* Fix - Blog layout issue of blog on Internet Explorer.
* Fix - Textarea's label is cut off.
* Fix - Page content is cut off when the page title is hidden.
* Fix - Checkout fields' style of Braintree payment gateways.
* Fix - Header text color option cannot be saved when using a custom header background color.
* Fix - Pay order page style issue.
* Fix - Variant option of typography settings works incorrectly when select "Italic".
* Fix - Topbar displays over the product gallery lightbox.
* Fix - Product Grid element doesn't display products on mobile when there is only 1 product.
* Fix - Hide the "--OR--" separator of Stripe buttons on the product page.
* Fix - Popup newsletter layout issue on mobile.

= Version 1.6.1 =
* Update - Update plugins.
* Update - Update WooCommerce templates to version 3.9.0.
* Improve - Related portfolio projects.
* Enhancement - Sale badge now can display the discount amount with {$} pattern.
* Fix - Page header text color issue.
* Fix - Blog page header search form doesn't work properly.
* Fix - Portfolio page header doesn't work with WPML.
* Fix - CSS issue of checkboxes.
* Fix - WPML doesn't detect campaign bar items automatically.
* Fix - Portfolio page doesn't use custom CSS of WPBakery Page Builder.
* Fix - CSS issue of WooCommerce breadcrumb with RTL languages.
* Dev - Add new filter for font loading URL.

= Version 1.6.0 =
* New - Add new options for menus.
* New - Add new option for cart panel width on mobile.
* Update - Update plugins.
* Update - Update WooCommerce templates to version 3.8.0.
* Enhancement - Support WhatsApp icon in the social menu.
* Fix - Issue of quantity dropdown.
* Fix - Apple pay button not showing.
* Fix - Product badge background setting is not applied on shop page.
* Fix - Typography settings for product page not working.
* Fix - Style issue of add-to-wishlist button.
* Fix - Style issue of search form on header.

= Version 1.5.0 =
* New - Support and include plugin YellowPencil to this theme. Plugin file is included in the theme package.
* New - Add new options to control product badges background color
* Update - Update plugins.
* Update - Update WooCommerce templates to version 3.7.0.
* Enhancement - Allows WPML to translate quick search links.
* Enhancement - Now can use ESC key to close modals, panels.
* Fix - Ajax add to cart on single product page doesn't trigger the event "added_to_cart" sometimes.
* Fix - Responsive issues of Instagram feed.
* Fix - Responsive issues of the mobile menu.
* Fix - Typography settings for product not working.
* Fix - Quick search issue when only one product returns.
* Fix - Wishlist counter calculates incorrectly when a product is removed from the wishlist.
* Fix - Incorrect related products on product layout v7.
* Fix - Error message with WP CLI.

= Version 1.4.1 =
* New - New option for the "Go to top" button at the footer.
* Fix - Instagram feed issue.

= Version 1.4.0 =
* Update - Update plugins.
* New - Add a new option to display product buttons on mobile.
* New - Support SoundCloud icon in the Social Menu.
* New - Support sharing via WhatsApp.
* Fix - Layout issue of product review form on mobile.
* Fix - Typography setting for widget title doesn't work.
* Fix - Incorrect products in side products section of product layout v7.
* Fix - Filter widget doesn't show up.
* Fix - Shop notification never disappear.
* Fix - Incorrect footer layout at 404 page.
* Fix - Warning of Instagram feed at footer.
* Fix - Issue of ajax add to cart on single product page.

= Version 1.3.2 =
* Improve - Security improvements.

= Version 1.3.1 =
* New - Add new option to turn product quick-view.
* Improve - Update RTL.
* Improve - Preview popup in Customizer.
* Improve - Preview preloader in Customizer.
* Fix - Instagram feed alignment issue.
* Fix - Typography settings for menu doesn't work with header v8 & v9.
* Fix - Responsiveness issues.
* Fix - Issue of normaly sticky header.

= Version 1.3.0 =
* New - Add "Size Guide" to the theme.
* Improve - Support purchase button for external products on wishlist.
* Fix - Error message when installing/upgrading WooCommerce plugin.
* Fix - Duplicated custom cart icon image.
* Fix - Social sharing icons are not clickable on mobile.
* Fix - Remove title attributes from SVG icons.
* Fix - SVG logo displays incorrectly.
* Fix - Layout issue of product layout v1.
* Fix - Always display "Sold Out" badge on variable products.
* Fix - Sometimes Instagram photos are not displayed.
* Fix - Product gallery issue on mobile with product layout v2 and v5.
* Fix - Incorrect style for product tabs element.
* Fix - Product Grid and Product Carousel elements cannot get featured products.

= Version 1.2.1 =
* Update - Update WooCommerce template files. Supports WooCommerce 3.6.x.
* Fix - Fix incorrect product category layout.
* Fix - Fix bug of product name on mini cart.
* Fix - Remove warning of Instagram footer photos feed on footer.
* Fix - Fix error with PHP 5.4

= Version 1.2.0 =
* New - Add new layout for the checkout page with 2 columns.
* New - Add campaign bar bellow the header.
* Update - Update plugins.
* Update - Update "Banner Imaeg" shortcode.

= Version 1.1.0 =
* New - Add Portfolio support
* New - Add new options to control product toolbar
* Update - Update WPML configuration file.
* Fix - Transparent header issue on Flex Posts template
* Fix - Transparent header issue on product page

= Version 1.0.2 =
* Fix - CSS issue of MailChimp checkbox.
* Fix - Responsiveness issues of Maintenance mode pages.
* Fix - Social share links don't work.

= Version 1.0.1 =
* Fix - error of custom color scheme
* Fix - CSS and responsiveness issues

= Version 1.0 =
* Initial release

== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
