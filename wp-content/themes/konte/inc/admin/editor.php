<?php
/**
 * Add more style to the edtior.
 *
 * @package Konte
 */

/**
 * Add dynamic style to editor.
 *
 * @param  string $mce
 * @return string
 */
function konte_editor_dynamic_styles( $mce ) {
    $styles = konte_typography_css();
    $styles = preg_replace( '/\s+/', ' ', $styles );
    $styles = str_replace( '"', '', $styles );

    if ( isset( $mce['content_style'] ) ) {
        $mce['content_style'] .= ' ' . $styles . ' ';
    } else {
        $mce['content_style'] = $styles . ' ';
    }

    return $mce;
}

add_filter( 'tiny_mce_before_init', 'konte_editor_dynamic_styles' );


/**
 * Enqueue editor styles for Gutenberg
 */
function konte_block_editor_styles() {
	wp_enqueue_style( 'konte-block-editor-style', get_theme_file_uri( '/css/style-block-editor.css' ), false, '1.0', 'all' );

	wp_add_inline_style( 'konte-block-editor-style', konte_typography_css() );
}
add_action( 'enqueue_block_editor_assets', 'konte_block_editor_styles' );