<?php
/**
 * Custom template tags of footer
 *
 * @package Konte
 */

function konte_footer_item( $item ) {
	switch ( $item ) {
		case 'copyright':
			echo '<div class="copyright">' . do_shortcode( wp_kses_post( konte_get_option( 'footer_copyright' ) ) ) . '</div>';
			break;

		case 'menu':
			if ( has_nav_menu( 'footer' ) ) {
				wp_nav_menu( array(
					'container'      => 'nav',
					'theme_location' => 'footer',
					'menu_id'        => 'footer-menu',
					'menu_class'     => 'footer-menu nav-menu menu',
					'depth'          => 1,
				) );
			}
			break;

		case 'social':
			if ( has_nav_menu( 'socials' ) ) {
				wp_nav_menu( array(
					'theme_location'  => 'socials',
					'container_class' => 'socials-menu ',
					'menu_id'         => 'footer-socials',
					'depth'           => 1,
					'link_before'     => '<span>',
					'link_after'      => '</span>',
				) );
			}
			break;

		case 'currency':
			konte_currency_switcher( array(
				'label'     => esc_html__( 'Currency', 'konte' ),
				'direction' => 'up',
			) );
			break;

		case 'language':
			konte_language_switcher( array(
				'label'     => esc_html__( 'Language', 'konte' ),
				'direction' => 'up',
			) );
			break;

		case 'currency_language':
			echo '<div class="switchers">';

			konte_currency_switcher( array(
				'label'     => esc_html__( 'Currency', 'konte' ),
				'direction' => 'up',
			) );
			konte_language_switcher( array(
				'label'     => esc_html__( 'Language', 'konte' ),
				'direction' => 'up',
			) );

			echo '</div>';
			break;

		case 'text':
			if ( $footer_custom_text = konte_get_option( 'footer_main_text' ) ) {
				echo '<div class="custom-text">' . do_shortcode( wp_kses_post( $footer_custom_text ) ) . '</div>';
			}
			break;

		default:
			do_action( 'konte_footer_footer_main_item', $item );
			break;
	}
}
